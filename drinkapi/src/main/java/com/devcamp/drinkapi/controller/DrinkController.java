package com.devcamp.drinkapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinkapi.model.Drink;
import com.devcamp.drinkapi.service.DrinkService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class DrinkController {
    @Autowired
    private DrinkService drinkService;

    @GetMapping("/drinks")
    public ArrayList<Drink> drinksList() {
        ArrayList<Drink> drinks = drinkService.allDrinks();
        return drinks;
    }

}
